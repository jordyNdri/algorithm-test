/**
 * APP ROUTING
 */
// No authentification
export const APP_ROUTE_ROOT = './';
/*export const APP_ROUTE_SIGN_UP = 'auth/sign-up';
export const APP_ROUTE_SIGN_IN = 'auth/sign-in';
export const APP_ROUTE_RESET_PASSWORD = 'reset-password';*/


// With authentification
export const APP_ROUTE_HOME = 'dashboard';

/** Routing Algorithm Test */
export const APP_ROUTE_TEST_UN = 'algorithm/test-un';
export const APP_ROUTE_TEST_DEUX = 'algorithm/test-deux';
export const APP_ROUTE_TEST_TROIS = 'algorithm/test-trois';
export const APP_ROUTE_TEST_QUATRE = 'algorithm/test-quatre';
export const APP_ROUTE_TEST_CINQ = 'algorithm/test-cinq';
export const APP_ROUTE_TEST_SIX = 'algorithm/test-six';
