import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TestQuatreComponent} from "./test-quatre.component";
import {RouterModule, Routes} from "@angular/router";
import {ReactiveFormsModule} from "@angular/forms";

const routes: Routes = [
  {
    path: '',
    component: TestQuatreComponent,
  }
];

@NgModule({
  declarations: [
    TestQuatreComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
  ],
  exports: [
    TestQuatreComponent,
    RouterModule,
  ],
})
export class TestQuatreModule {
}
