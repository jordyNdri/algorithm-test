import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TestQuatreComponent } from './test-quatre.component';

describe('TestQuatreComponent', () => {
  let component: TestQuatreComponent;
  let fixture: ComponentFixture<TestQuatreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TestQuatreComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestQuatreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
