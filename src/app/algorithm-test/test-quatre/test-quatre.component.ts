import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {APP_ROUTE_HOME} from "../../shared/constants/routing.constant";

@Component({
  selector: 'app-test-deux',
  templateUrl: './test-quatre.component.html',
  styleUrls: ['./test-quatre.component.scss']
})
export class TestQuatreComponent implements OnInit {
  descripObjetSource = [
    {nom: 'Albert', age: 12, diplome: 'CEPE'},
    {nom: 'Bernard', age: 16, diplome: 'BEPC'},
  ];

  descripObjetCible = {
    'Albert': {
      age: 12,
      diplome: 'CEPE'
    },
    'Bernard': {
      age: 16,
      diplome: 'BEPC'
    },
  };

  objetSource: { nom: string; age: number; diplome: string; }[] = [];
  objetCible: any = {};

  ifActionConvertir: boolean = false;

  constructor(
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    this.objetSource = this.descripObjetSource;
  }

  async goBack() {
    await this.router.navigate([`/${APP_ROUTE_HOME}`]);
  }

  convertir() {
    this.ifActionConvertir = true;

    /**
     * Code
     */
    this.objetCible = this.objetSource.reduce((result, item, index, array) => {
      return {[item.nom]: item, ...result};
    }, {});

    setTimeout(() => {
      // @ts-ignore
      document.getElementById("view-code").innerHTML =
        `
  this.objetCible = this.objetSource.reduce((result, item, index, array) => {
    return {[item.nom]: item, ...result};
  }, {});
      `;
    }, 1000);
  }

}
