import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {APP_ROUTE_HOME} from "../../shared/constants/routing.constant";

interface InterfaceSource {
  id: string;
  name: string;
  children?: InterfaceSource[]
}

interface InterfaceCible {
  parentId: any;
  id: string;
  name: string;
}

@Component({
  selector: 'app-test-deux',
  templateUrl: './test-cinq.component.html',
  styleUrls: ['./test-cinq.component.scss']
})
export class TestCinqComponent implements OnInit {
  descripObjetSource = [
    {
      id: 'element1',
      name: 'element 1',
      children: [
        {
          id: 'element11',
          name: 'element 11',
        },
        {
          id: 'element12',
          name: 'element 12',
          children: [
            {
              id: 'element121',
              name: 'element 121',
            },
            {
              id: 'element122',
              name: 'element 122',
            },
          ]
        },
      ]
    },
    {
      id: 'element2',
      name: 'element 2',
    }
  ];

  descripObjetCible = [
    {
      parentId: null,
      id: 'element1',
      name: 'element 1',
    },
    {
      parentId: null,
      id: 'element2',
      name: 'element 2',
    },
    {
      parentId: 'element1',
      id: 'element11',
      name: 'element 11',
    },
    {
      parentId: 'element12',
      id: 'element121',
      name: 'element 121',
    },
  ];

  objetSource: InterfaceSource[] = [];
  objetCible: InterfaceCible[] = [];

  ifActionConvertir: boolean = false;

  constructor(
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    this.objetSource = this.descripObjetSource;
  }

  async goBack() {
    await this.router.navigate([`/${APP_ROUTE_HOME}`]);
  }

  convertir() {
    this.ifActionConvertir = true;

    /**
     * Code
     */
    this.objetSource.map((item) => {
      this.objetCible.push({parentId: null, id: item.id, name: item.name});

      if (item.children) {
        item.children.map((child) => {
          this.objetCible.push({parentId: item.id, id: child.id, name: child.name});

          if (child.children) {
            child.children.map((child_2) => {
              this.objetCible.push({parentId: child.id, id: child_2.id, name: child_2.name});
            });
          }
        });
      }
    });

    setTimeout(() => {
      // @ts-ignore
      document.getElementById("view-code").innerHTML =
        `
  this.objetSource.map((item) => {
    this.objetCible.push({parentId: null, id: item.id, name: item.name});

    if (item.children) {
      item.children.map((child) => {
        this.objetCible.push({parentId: item.id, id: child.id, name: child.name});

        if (child.children) {
          child.children.map((child_2) => {
            this.objetCible.push({parentId: child.id, id: child_2.id, name: child_2.name});
          });
        }
      });
    }
  });
      `;
    }, 1000);
  }

}
