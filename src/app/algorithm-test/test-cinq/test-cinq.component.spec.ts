import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TestCinqComponent } from './test-cinq.component';

describe('TestCinqComponent', () => {
  let component: TestCinqComponent;
  let fixture: ComponentFixture<TestCinqComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TestCinqComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestCinqComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
