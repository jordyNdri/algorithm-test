import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TestCinqComponent} from "./test-cinq.component";
import {RouterModule, Routes} from "@angular/router";
import {ReactiveFormsModule} from "@angular/forms";

const routes: Routes = [
  {
    path: '',
    component: TestCinqComponent,
  }
];

@NgModule({
  declarations: [
    TestCinqComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
  ],
  exports: [
    TestCinqComponent,
    RouterModule,
  ],
})
export class TestCinqModule {
}
