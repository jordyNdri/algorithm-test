import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TestTroisComponent } from './test-trois.component';

describe('TestTroisComponent', () => {
  let component: TestTroisComponent;
  let fixture: ComponentFixture<TestTroisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TestTroisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestTroisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
