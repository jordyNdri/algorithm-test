import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {APP_ROUTE_HOME} from "../../shared/constants/routing.constant";

@Component({
  selector: 'app-test-deux',
  templateUrl: './test-trois.component.html',
  styleUrls: ['./test-trois.component.scss']
})
export class TestTroisComponent implements OnInit {
  descripObjetSource = {
    'Albert': {
      age: 12,
      diplome: 'CEPE'
    },
    'Bernard': {
      age: 16,
      diplome: 'BEPC'
    },
  };

  descripObjetCible = [
    {nom: 'Albert', age: 12, diplome: 'CEPE'},
    {nom: 'Bernard', age: 16, diplome: 'BEPC'},
  ];

  objetSource: any = {};
  objetCible: { nom: string; age: number; diplome: string; }[] = [];

  ifActionConvertir: boolean = false;

  constructor(
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    this.objetSource = this.descripObjetSource;
  }

  async goBack() {
    await this.router.navigate([`/${APP_ROUTE_HOME}`]);
  }

  convertir() {
    this.ifActionConvertir = true;

    /**
     * Code 1
     */
    Object.entries(this.objetSource).map((item) => {
      let data: any = item[1];
      data = {...data, nom: item[0]}
      this.objetCible.push(data);
    });

    /**
     * Code 2 => Ce code ne fonctionnera pas si "objetSource" est de type "object"
     */
    Object.keys(this.objetSource).map((item) => {
      const data = {nom: item, ...this.objetSource[item]}
      this.objetCible.push(data)
    });

    setTimeout(() => {
      // @ts-ignore
      document.getElementById("view-code-1").innerHTML =
        `
  /** Code 1 */
  Object.entries(this.objetSource).map((item) => {
    let data: any = item[1];
    data = {...data, nom: item[0]}
    this.objetCible.push(data);
  });
      `;

      // @ts-ignore
      document.getElementById("view-code-2").innerHTML =
        `
  /** Code 2 => Ce code ne fonctionnera pas si "objetSource" est de type "object" */
  Object.keys(this.objetSource).map((item) => {
    const data = {nom: item, ...this.objetSource[item]}
    this.objetCible.push(data)
  });
      `;
    }, 1000);

  }

}
