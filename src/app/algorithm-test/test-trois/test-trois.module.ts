import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TestTroisComponent} from "./test-trois.component";
import {RouterModule, Routes} from "@angular/router";
import {ReactiveFormsModule} from "@angular/forms";

const routes: Routes = [
  {
    path: '',
    component: TestTroisComponent,
  }
];

@NgModule({
  declarations: [
    TestTroisComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
  ],
  exports: [
    TestTroisComponent,
    RouterModule,
  ],
})
export class TestTroisModule {
}
