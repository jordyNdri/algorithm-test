import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TestUnComponent} from "./test-un.component";
import {RouterModule, Routes} from "@angular/router";
import {ReactiveFormsModule} from "@angular/forms";

const routes: Routes = [
  {
    path: '',
    component: TestUnComponent,
  }
];

@NgModule({
  declarations: [
    TestUnComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
  ],
  exports: [
    TestUnComponent,
    RouterModule,
  ],
})
export class TestUnModule {
}
