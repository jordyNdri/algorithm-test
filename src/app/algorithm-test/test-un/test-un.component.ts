import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {APP_ROUTE_HOME} from "../../shared/constants/routing.constant";

@Component({
  selector: 'app-test-un',
  templateUrl: './test-un.component.html',
  styleUrls: ['./test-un.component.scss']
})
export class TestUnComponent implements OnInit {

  formGroup: FormGroup;

  message: { type: boolean; text: string; } = {text: '', type: false};

  dateValide: { type: boolean; text: string; } = {type: true, text: `La date est valide !`};
  dateInvalide: { type: boolean; text: string; } = {type: false, text: `La date n'est pas valide !`};

  maxDay: number = 31;
  maxMonth: number = 12;
  maxYear: number = 2022;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
  ) {
    this.formGroup = this.initialiserFormulaire(null);
  }

  ngOnInit(): void {
    this.formGroup = this.initialiserFormulaire(null)
  }

  initialiserFormulaire(datas: any, args?: any): FormGroup {
    const formConstruct: any = {
      day: !!datas && !!datas.day ? datas.day : null,
      month: !!datas && !!datas.month ? datas.month : null,
      year: !!datas && !!datas.year ? datas.year : null,
    };

    return this.formBuilder.group({
      day: [{value: formConstruct.day, disabled: false}, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(2),
        // Validators.pattern(`${REGEX_PATTERN.textarea}{5,200}$`)
      ]],
      month: [{value: formConstruct.month, disabled: false}, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(2),
        // Validators.pattern(`${REGEX_PATTERN.textarea}{5,200}$`)
      ]],
      year: [{value: formConstruct.year, disabled: false}, [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(4),
        // Validators.pattern(`${REGEX_PATTERN.textarea}{5,200}$`)
      ]],
    });
  }

  saveFormulaire() {
    const inputDay = this.formGroup.getRawValue().day;
    const inputMonth = this.formGroup.getRawValue().month;
    const inputYear = this.formGroup.getRawValue().year;

    if (inputDay > this.maxDay) {
      this.message = this.dateInvalide;
      return;
    }

    if (inputMonth > this.maxMonth) {
      this.message = this.dateInvalide;
      return;
    }

    /**
     * Limit year
     */
    if (inputYear > this.maxYear) {
      this.message = this.dateInvalide;
      return;
    }

    if (inputDay > 30 &&
      (inputMonth !== 1 || inputMonth !== 3 || inputMonth !== 5 || inputMonth !== 7 || inputMonth !== 8 || inputMonth !== 10 || inputMonth !== 12)) {
      this.message = this.dateInvalide;
      return;
    }

    if (inputMonth === 2) {
      if (inputDay > 29) {
        this.message = this.dateInvalide;
        return;
      }
      if (inputDay === 29 && this.isFloat((inputYear / 4))) {
        this.message = this.dateInvalide;
        return;
      }
    }

    this.message = this.dateValide;
  }

  async goBack() {
    await this.router.navigate([`/${APP_ROUTE_HOME}`]);
  }

  isInt(n: any) {
    return Number(n) === n && n % 1 === 0;
  }

  isFloat(n: any) {
    return Number(n) === n && n % 1 !== 0;
  }
}
