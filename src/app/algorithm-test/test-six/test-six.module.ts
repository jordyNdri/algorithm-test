import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TestSixComponent} from "./test-six.component";
import {RouterModule, Routes} from "@angular/router";
import {ReactiveFormsModule} from "@angular/forms";

const routes: Routes = [
  {
    path: '',
    component: TestSixComponent,
  }
];

@NgModule({
  declarations: [
    TestSixComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
  ],
  exports: [
    TestSixComponent,
    RouterModule,
  ],
})
export class TestSixModule {
}
