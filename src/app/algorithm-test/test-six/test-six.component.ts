import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {APP_ROUTE_HOME} from "../../shared/constants/routing.constant";

interface InterfaceSource {
  parentId: any;
  id: string;
  name: string;
}

interface InterfaceCible {
  id: string;
  name: string;
  children?: InterfaceCible[]
}

@Component({
  selector: 'app-test-deux',
  templateUrl: './test-six.component.html',
  styleUrls: ['./test-six.component.scss']
})
export class TestSixComponent implements OnInit {
  descripObjetSource = [
    {
      parentId: null,
      id: 'element1',
      name: 'element 1',
    },
    {
      parentId: null,
      id: 'element2',
      name: 'element 2',
    },
    {
      parentId: 'element1',
      id: 'element11',
      name: 'element 11',
    },
    {
      parentId: 'element12',
      id: 'element121',
      name: 'element 121',
    },
  ];

  descripObjetCible = [
    {
      id: 'element1',
      name: 'element 1',
      children: [
        {
          id: 'element11',
          name: 'element 11',
        },
        {
          id: 'element12',
          name: 'element 12',
          children: [
            {
              id: 'element121',
              name: 'element 121',
            },
            {
              id: 'element122',
              name: 'element 122',
            },
          ]
        },
      ]
    },
    {
      id: 'element2',
      name: 'element 2',
    }
  ];

  objetSource: InterfaceSource[] = [];
  objetCible: InterfaceCible[] = [];

  ifActionConvertir: boolean = false;

  constructor(
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    this.objetSource = this.descripObjetSource;
  }

  async goBack() {
    await this.router.navigate([`/${APP_ROUTE_HOME}`]);
  }

  convertir() {
    this.ifActionConvertir = true;

    /**
     * Code
     */

    setTimeout(() => {
      if (this.ifActionConvertir) {
        // @ts-ignore
        document.getElementById("view-code").innerHTML =
          `
          `;
      }
    }, 1000);
  }

}
