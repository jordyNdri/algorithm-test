import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TestDeuxComponent} from "./test-deux.component";
import {RouterModule, Routes} from "@angular/router";
import {ReactiveFormsModule} from "@angular/forms";

const routes: Routes = [
  {
    path: '',
    component: TestDeuxComponent,
  }
];

@NgModule({
  declarations: [
    TestDeuxComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
  ],
  exports: [
    TestDeuxComponent,
    RouterModule,
  ],
})
export class TestDeuxModule {
}
