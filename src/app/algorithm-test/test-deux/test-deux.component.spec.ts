import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TestDeuxComponent } from './test-deux.component';

describe('TestDeuxComponent', () => {
  let component: TestDeuxComponent;
  let fixture: ComponentFixture<TestDeuxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TestDeuxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestDeuxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
