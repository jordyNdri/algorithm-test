import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {APP_ROUTE_HOME} from "../../shared/constants/routing.constant";

@Component({
  selector: 'app-test-deux',
  templateUrl: './test-deux.component.html',
  styleUrls: ['./test-deux.component.scss']
})
export class TestDeuxComponent implements OnInit {

  formGroup: FormGroup;

  listValue: number[] = [];

  message: { type: boolean; text: string; } = {text: '', type: false};

  valueNotExist: { type: boolean; text: string; } = {
    type: true,
    text: `La dernière valeur n'a pas encore été saisie !`
  };
  valueExist: { type: boolean; text: string; } = {type: false, text: `La dernière valeur a déjà été saisie !`};

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
  ) {
    this.formGroup = this.initialiserFormulaire(null);
  }

  ngOnInit(): void {
    this.formGroup = this.initialiserFormulaire(null)
  }

  initialiserFormulaire(datas: any, args?: any): FormGroup {
    const formConstruct: any = {
      value: !!datas && !!datas.value ? datas.value : null,
    };

    return this.formBuilder.group({
      value: [{value: formConstruct.value, disabled: false}, [
        Validators.required,
        Validators.minLength(1),
        // Validators.maxLength(2),
        // Validators.pattern(`${REGEX_PATTERN.textarea}{5,200}$`)
      ]],
    });
  }

  saveFormulaire() {
    const inputValue = this.formGroup.getRawValue().value;

    if ((typeof inputValue) !== 'number') {
      this.message = {type: true, text: `La valeur saisie n'est pas un nombre !`}
    }

    if (this.listValue.length === 20) {
      if (this.listValue.includes(inputValue)) {
        this.message = this.valueExist;
        return;
      }

      this.message = this.valueNotExist;
      return;

    } else {
      this.listValue.push(inputValue);
    }

    this.formGroup.patchValue({value: null});
  }

  async goBack() {
    await this.router.navigate([`/${APP_ROUTE_HOME}`]);
  }

  resetFormulaire() {
    this.listValue = [];
    this.formGroup.reset();
  }
}
