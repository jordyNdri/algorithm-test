import {Component, OnInit} from '@angular/core';
import {
  APP_ROUTE_TEST_CINQ,
  APP_ROUTE_TEST_DEUX,
  APP_ROUTE_TEST_QUATRE, APP_ROUTE_TEST_SIX,
  APP_ROUTE_TEST_TROIS,
  APP_ROUTE_TEST_UN
} from "../shared/constants/routing.constant";
import {Router} from "@angular/router";

interface TestInterface {
  name: string;
  root: string;
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  title = 'ALGORITHM TEST';

  ListAlgorithmTestRessource: TestInterface[] = [];

  constructor(
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    this.ListAlgorithmTestRessource = [
      {name: 'Test 1', root: `${APP_ROUTE_TEST_UN}`},
      {name: 'Test 2', root: `${APP_ROUTE_TEST_DEUX}`},
      {name: 'Test 3', root: `${APP_ROUTE_TEST_TROIS}`},
      {name: 'Test 4', root: `${APP_ROUTE_TEST_QUATRE}`},
      {name: 'Test 5', root: `${APP_ROUTE_TEST_CINQ}`},
      {name: 'Test 6', root: `${APP_ROUTE_TEST_SIX}`},
    ];
  }

  async goTo(link: string) {
    await this.router.navigate([`/${link}`])
  }

}
