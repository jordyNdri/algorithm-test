import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {DashboardComponent} from "./dashboard.component";

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    /*children: [
      {
        path: `${APP_ROUTE_TEST_UN}`,
        loadChildren: () => import('./type-abonnement/type-abonnement.module').then(module => module.TypeAbonnementModule)
      },
    ]*/
  }
];

@NgModule({
  declarations: [
    DashboardComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    DashboardComponent,
    RouterModule,
  ]
})
export class DashboardModule {
}
