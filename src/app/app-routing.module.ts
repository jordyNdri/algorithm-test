import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {
  APP_ROUTE_HOME, APP_ROUTE_TEST_CINQ,
  APP_ROUTE_TEST_DEUX, APP_ROUTE_TEST_QUATRE, APP_ROUTE_TEST_SIX,
  APP_ROUTE_TEST_TROIS,
  APP_ROUTE_TEST_UN
} from "./shared/constants/routing.constant";

const routes: Routes = [
  {
    path: '',
    redirectTo: `${APP_ROUTE_HOME}`,
    pathMatch: 'full'
  },
  {
    path: `${APP_ROUTE_HOME}`,
    // canActivate: [AuthenticationGuard],
    loadChildren: () => import('./dashboard/dashboard.module').then(module => module.DashboardModule)
  },
  {
    path: `${APP_ROUTE_TEST_UN}`,
    loadChildren: () => import('./algorithm-test/test-un/test-un.module').then(module => module.TestUnModule)
  },
  {
    path: `${APP_ROUTE_TEST_DEUX}`,
    loadChildren: () => import('./algorithm-test/test-deux/test-deux.module').then(module => module.TestDeuxModule)
  },
  {
    path: `${APP_ROUTE_TEST_TROIS}`,
    loadChildren: () => import('./algorithm-test/test-trois/test-trois.module').then(module => module.TestTroisModule)
  },
  {
    path: `${APP_ROUTE_TEST_QUATRE}`,
    loadChildren: () => import('./algorithm-test/test-quatre/test-quatre.module').then(module => module.TestQuatreModule)
  },
  {
    path: `${APP_ROUTE_TEST_CINQ}`,
    loadChildren: () => import('./algorithm-test/test-cinq/test-cinq.module').then(module => module.TestCinqModule)
  },
  {
    path: `${APP_ROUTE_TEST_SIX}`,
    loadChildren: () => import('./algorithm-test/test-six/test-six.module').then(module => module.TestSixModule)
  },
  {
    path: '**',
    loadChildren: () => import('./dashboard/dashboard.module').then(module => module.DashboardModule)
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
